<?php
/**
 * Реализовать возможность входа с паролем и логином с использованием
 * сессии для изменения отправленных данных в предыдущей задаче,
 * пароль и логин генерируются автоматически при первоначальной отправке формы.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// Массив суперспособностей
$abilities = array(
    'immort' => "Бессмертие",
    'wall' => "Прохождение сквозь стены",
    'levit' => "Левитация",
    'invis' => "Невидимость");

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
// Выдаём форму методом GET
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // Массив для временного хранения сообщений пользователю.
    $messages = array();
    // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
    // Выдаем сообщение об успешном сохранении.
    if (!empty($_COOKIE['save'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('save', '', 1);
        setcookie('login', '', 1);
        setcookie('pass', '', 1);
        // Выводим сообщение пользователю.
        $messages[] = 'Спасибо, результаты сохранены.';
        // Если в куках есть пароль, то выводим сообщение.
        if (!empty($_COOKIE['pass'])) {
            $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
            и паролем <strong>%s</strong> для изменения данных.',
            strip_tags($_COOKIE['login']),
            strip_tags($_COOKIE['pass']));
        }
    }

    // Складываем признак ошибок в массив.
    $errors = array();
    $errors['FIO'] = !empty($_COOKIE['FIO_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['yob'] = !empty($_COOKIE['yob_error']);
    $errors['gender'] = !empty($_COOKIE['gender_error']);
    $errors['_value'] = !empty($_COOKIE['_value_error']);
    $errors['abilities'] = !empty($_COOKIE['abilities_error']);
    $errors['text'] = !empty($_COOKIE['text_error']);
    $errors['accept'] = !empty($_COOKIE['accept_error']);

    // Выдаем сообщения об ошибках
    // FIO
    if ($errors['FIO']) {
        // Удаляем куку, указывая время устаревания в прошлом
        setcookie('FIO_error', '', 1);
        // Выводим сообщение
        if ($_COOKIE['FIO_error'] == "1") {
            $messages[] = '<div class="error">Заполните имя</div>';
        }
        else {
            $messages[] = '<div class="error">Укажите корректное имя</div>';
        }
    }

    // email
    if ($errors['email']) {
        // Удаляем куку, указывая время устаревания в прошлом
        setcookie('email_error', '', 1);
        // Выводим сообщение
        if ($_COOKIE['email_error'] == "1") {
            $messages[] = '<div class="error">Заполните email</div>';
        }
        else {
            $messages[] = '<div class="error">Укажите корректный email</div>';
        }
    }

    // yob
    if ($errors['yob']) {
        // Удаляем куку, указывая время устаревания в прошлом
        setcookie('yob_error', '', 1);
        // Выводим сообщение
        if ($_COOKIE['yob_error'] == "1") {
            $messages[] = '<div class="error">Заполните год</div>';
        }
        else {
            $messages[] = '<div class="error">Укажите корректный год</div>';
        }
    }
    
    // accept
    if ($errors['accept']) {
        // Удаляем куку, указывая время устаревания в прошлом
        setcookie('accept_error', '', 1);
        // Выводим сообщение
        if ($_COOKIE['accept_error'] == "1") {
            $messages[] = '<div class="error">Вы не приняли соглашение</div>';
        }
    }

     // gender
     if ($errors['gender']) {
        // Удаляем куку, указывая время устаревания в прошлом
        setcookie('genser_error', '', 1);
        // Выводим сообщение
        if ($_COOKIE['gender_error'] == "1") {
            $messages[] = '<div class="error">Укажите пол</div>';
        }
    }

    // text
    if ($errors['text']) {
        // Удаляем куку, указывая время устаревания в прошлом
        setcookie('text_error', '', 1);
        // Выводим сообщение
        if ($_COOKIE['text_error'] == "1") {
            $messages[] = '<div class="error">Заполните текстовое поле</div>';
        }
    }

    // abilities
    if ($errors['abilities']) {
      // Удаляем куку, указывая время устаревания в прошлом
      setcookie('abilities_error', '', 1);
      // Выводим сообщение
      if ($_COOKIE['abilities_error'] == "1") {
          $messages[] = '<div class="error">Выберите споособность</div>';
      }
      else {
          $messages[] = '<div class="error">Выбрана недопустимая способность</div>';
      }
    }


    // Складываем предыдущие значения полей в массив, если есть.
    // При этом санитизуем все данные для безопасного отображения в браузере.
    $values = array();
    $values['FIO'] = empty($_COOKIE['FIO_value']) || !preg_match('/^[a-zA-Zа-яёА-ЯЁ\s\-]+$/u', $_COOKIE['FIO_value']) ? '' : $_COOKIE['FIO_value'];
    $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
    $values['yob'] = empty($_COOKIE['yob_value']) ? '' : $_COOKIE['yob_value'];
    $values['gender'] = $_COOKIE['gender_value'] = '0' ? 0 : 1;
    $values['_value'] = empty($_COOKIE['_value_value']) ? '' : $_COOKIE['_value_value'];
    if (!empty($_COOKIE['abilities_value'])) {
        $abilities_value = json_decode($_COOKIE['abilities_value']);
    }
    $values['abilities'] = array();
    if (is_array($abilities_value)) {
        foreach($abilities_value as $ability) {
            if (!empty($abilities[$ability])) {
                $values['abilities'][$ability] = $ability;
            }
        }
    }
    $values['text'] = empty($_COOKIE['text_value']) ? '' : $_COOKIE['text_value'];
    $values['accept'] = empty($_COOKIE['accept_value']) ? '' : $_COOKIE['accept_value'];

    // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
    // ранее в сессию записан факт успешного логина.
    if (empty($errors) && !empty($_COOKIE[session_name()]) && session_start() && !empty($_SESSION['id_user'])) {
        // TODO: загрузить данные пользователя из БД
        // и заполнить переменную $values,
        printf('Вход с логином %s, id %d', $_SESSION['login'], $_SESSION['id_user']);
    }

    // Включаем содержимое файла form.php.
    // В нем будут доступны переменные $messages, $errors и $values для вывода 
    // сообщений, полей с ранее заполненными данными и признаками ошибок.
    include('form.php');
}
// Иначе, если запрос был методом POST
// Обрабатываем сохранения
else {
    // Валидация формы
    $errors = FALSE;

    // FIO
    if (empty($_POST['FIO'])) {
        setcookie('FIO_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        if (!preg_match('/^[a-zA-Zа-яёА-ЯЁ\s\-]+$/u', $_POST['FIO'])) {
        setcookie('FIO_error', '2', time() + 24 * 60 * 60);
        $errors = TRUE;
        }
        // Сохраняем ранее введенное в форму значение на год
        setcookie('FIO_value', $_POST['FIO'], time() + 12 * 30 * 24 * 60 * 60);
    }

    // email
    if (empty($_POST['email'])) {
        // Выдаем куку на день с флажком об ошибке в поле email.
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        if (!preg_match('/^[^@]+@[^@.]+\.[^@]+$/', $_POST['email'])) {
        setcookie('email_error', '2', time() + 24 * 60 * 60);
        $errors = TRUE;
        }
        // Сохраняем ранее введенное в форму значение на год
        setcookie('email_value', $_POST['email'], time() + 12 * 30 * 24 * 60 * 60);
    }

    // yob
    if (empty($_POST['yob'])) {
        setcookie('yob_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        $yob = $_POST['yob'];
        if (!(is_numeric($yob) && intval($yob) >= 1900 && intval($yob) <= 2020)) {
        setcookie('yob_error', '2', time() + 24 * 60 * 60);  
            $errors = TRUE;
        }
        setcookie('yob_value', $_POST['yob'], time() + 12 * 30 * 24 * 60 * 60);
    }

    // gender
    setcookie('gender_value', $_POST['gender'], time() + 12 * 30 * 24 * 60 * 60);

    // _value
    if (empty($_POST['_value'])) {
        setcookie('_value_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на год
        setcookie('_value_value', $_POST['_value'], time() + 12 * 30 * 24 * 60 * 60);
    }

    // abilities
    if (empty($_POST['abilities'])) {
        setcookie('abilities_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        $abilities_error = FALSE;
        foreach($_POST['abilities'] as $key) {
            if (empty($abilities[$key])) {
                setcookie('abilities_error', '2', time() + 24 * 60 * 60);
                $errors = TRUE;
                $abilities_error = TRUE;
            }
        }
        if (!$abilities_error) {
            setcookie('abilities_value', json_encode($_POST['abilities']), time() + 12 * 30 * 24 * 60 * 60);
        }
    }

    // text
    if (empty($_POST['text'])) {
        setcookie('text_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('text_value', $_POST['text'], time() + 12 * 30 * 24 * 60 * 60);
    }

    // accept
    if (!isset($_POST['accept'])) {
        setcookie('accept_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('accept_value', $_POST['accept'], time() + 12 * 30 * 24 * 60 * 60);
    }

    $ability_data = ['immort', 'wall', 'levit', 'invis'];
    $abil = $_POST['abilities'];
    // print($values['abilities']);
    foreach($ability_data as $ability) {
        $ability_insert[$ability] = in_array($ability, $abil) ? 1 : 0;
    }

    // Проверяем массив ошибок
    if ($errors) {
        // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
        header('Location: index.php');
        exit();
    }
    else {
        // Удаляем Cookies с признаками ошибок.
        setcookie('FIO_error', '', 1);
        setcookie('email_error', '', 1);
        setcookie('yob_error', '', 1);
        setcookie('gender_error', '', 1);
        setcookie('_value_error', '', 1);
        setcookie('abilities_error', '', 1);
        setcookie('text_error', '', 1);
        setcookie('accept_error', '', 1);
    }

    // Соединение с БД
    include("bd.php");

    // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
    // Пользователь уже известен
    if (!empty($_COOKIE[session_name()]) && session_start() && !empty($_SESSION['id_user'])) {
        $stmt = $db->prepare("UPDATE `Users` SET `FIO` = ?, `email` = ?, `yob` = ?, `gender` = ?, `_value` = ?, `immort` = ?, `wall` = ?, `levit` = ?, `invis` = ?, `text` = ?, `accept` = ? WHERE `id_user` = ?");
        $stmt->execute(array($_POST['FIO'], $_POST['email'], intval($_POST['yob']), intval($_POST['gender']), intval($_POST['_value']), intval($ability_insert['immort']), intval($ability_insert['wall']), intval($ability_insert['levit']), intval($ability_insert['invis']), $_POST['text'], 1, $_SESSION['login']));
    }
    // Новый пользователь
    else {
        exit("Для того, чтобы отправить форму, нужно зарегистрироваться!");
    }
    
    // Сохраняем куку с признаком успешного сохранения.
    setcookie('save', '1');
    // Делаем перенаправление.
    header('Location: ./');
}
