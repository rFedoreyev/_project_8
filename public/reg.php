<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Регистрация</title>
	<style>
		form {
			width: 600px;
			height: 200px;
			background: white;
			border-radius: 8px;
			margin: 0 auto;
			padding: 30px;
			padding-bottom: 60px;
			box-shadow: 0px 0px 14px 0px rgba(46, 53, 55, 0.77);
		}
		label {
	       	margin: 3px;
	   	}
		input {
	       	margin: 8px 0;
	   	}
		input[type="text"], input[type="password"] {
			width: 100%;
			height: 30px;
			border-radius: 5px;
			outline: none;
			padding: 7px;
	   	}
		input[type="submit"] {
			padding: 7px 20px;
			border-radius: 5px;
			box-shadow: 0px 0px 5px 0px rgba(46, 53, 55, 0.5);
		}
		input[type="submit"]:hover {
			cursor: pointer;
		}
		.error {
			border: 1px solid red;
		}
	</style>
</head>
<body>
	<form action="save_user.php" method="POST">
		<label>Ваш логин</label>
		<input name="login" type="text">
		<label>Ваш пароль</label>
		<input name="pass" type="password">
		<input type="submit" value="Зарегистрироваться" name="submit">
	</form>
</body>
</html>